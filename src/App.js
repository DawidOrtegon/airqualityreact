import React, {Component} from "react";
import AirQuality from "./AirQuality";


// Clase desde donde se llaman los componentes para ser mostrados en la pagina.
class App extends Component
{
    constructor() {
        super();
        this.state = {PM10:null,PM25:null};
    }


    componentDidMount()
    {
        this.setState({PM10:getRandomArbitrary(2.0, 200.0), PM25:getRandomArbitrary(2,170)})
        console.log("Component was rendered");
    }

    render()
    {
        return(
            <div>
                <h1> Air Quality :)</h1>
                <div>
                    <AirQuality PM10={this.state.PM10} PM25={this.state.PM25}></AirQuality>
                </div>
            </div>
        )
    }
}

function getRandomArbitrary(min, max)
{
    return (Math.random() * (max - min + min) + min).toFixed(2);
}

export default App;
