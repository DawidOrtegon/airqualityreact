import React from "react";
import './AirQuality.css';
import './Ideal.png';
import './Good.png';
import './Average.png';
import './Neutral.png';
import './Bad.png';
import './Worst.png';


// Componentes para el procesamiento de los datos del aire y retornar la calidad.

const getConditionTextPM10 = (PM10Value) =>
{
    if (PM10Value >= 2 && PM10Value <= 20)
    {
        return 'Ideal';
    }
    else if (PM10Value >= 20.1 && PM10Value <= 50)
    {
        return 'Good';
    }
    else if (PM10Value >= 50.1 && PM10Value <= 80)
    {
        return 'Average';
    }
    else if (PM10Value >= 80.1 && PM10Value <= 110)
    {
        return 'Neutral';
    }
    else if (PM10Value >= 110.1 && PM10Value <= 150)
    {
        return 'Bad';
    }
    else if (PM10Value > 150)
    {
        return 'Worst';
    }
};

const setEmoticonPM10 =
    {
        Ideal: {
            iconPM10: './Ideal.png'
        },
        Good: {
            iconPM10: './Good.png'
        },
        Average: {
            iconPM10: './Average.png'
        },
        Neutral: {
            iconPM10: './Neutral.png'
        },
        Bad: {
            iconPM10: './Bad.png'
        },
        Worst: {
            iconPM10: './Worst.png'
        }
    };

const getConditionTextPM25 = (PM25Value) =>
{
    if (PM25Value >= 2 && PM25Value <= 13)
    {
        return 'Ideal';
    }
    else if (PM25Value >= 13.1 && PM25Value <= 35)
    {
        return 'Good';
    }
    else if(PM25Value >= 35.1 && PM25Value <= 55)
    {
        return 'Average';
    }
    else if (PM25Value >= 55.1 && PM25Value <= 75)
    {
        return 'Neutral';
    }
    else if (PM25Value >= 75.1 && PM25Value <= 110)
    {
        return 'Bad';
    }
    else if (PM25Value > 110)
    {
        return 'Worst';
    }

};

const setEmoticonPM25 =
{
    Ideal: {
        iconPM25: './Ideal.png'
    },
    Good: {
        iconPM25: './Good.png'
    },
    Average: {
        iconPM25: './Average.png'
    },
    Neutral: {
        iconPM25: './Neutral.png'
    },
    Bad: {
        iconPM25: './Bad.png'
    },
    Worst: {
        iconPM25: './Worst.png'
    }
};

const airQualityScreen = props =>
{
    const conditionTextPM10 = getConditionTextPM10(props.PM10);
    console.log(conditionTextPM10);
    const iconPM10 = setEmoticonPM10[getConditionTextPM10(props.PM10)];
    console.log(iconPM10)

    const conditionTextPM25 = getConditionTextPM25(props.PM25);
    console.log(conditionTextPM25);
    const iconPM25 = setEmoticonPM25[getConditionTextPM25(props.PM25)];
    console.log(iconPM25)

    return(
        <div>
            <div className={'condition-display ${conditionTextPM10}'} >
                <h2>PM10: {props.PM10} µg/m3 </h2>
                <img src={iconPM10} className={'center'}  alt={"iconPM10"}/>
                <p> {conditionTextPM10} </p>
            </div>

            <div className={'condition-display ${conditionTextPM25}'}>
                <h2>PM25: {props.PM25} µg/m3 </h2>
                <img src={iconPM25} className={'center'} alt={"iconPM25"}/>
                <p> {conditionTextPM25} </p>
            </div>
        </div>
    );

}

export default airQualityScreen;


